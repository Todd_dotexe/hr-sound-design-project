﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using System;
using UnityEngine;

public class PickupManager : MonoBehaviour
{
    public GameObject Axe;
    public GameObject Bucket;
    public GameObject Berry;
    public GameObject FullBucket;
    public GameObject Rubble;
    public GameObject Throw;

    bool AxeSound;
    bool BerrySound;
    bool BucketSound;
    bool FullBucketSound;
    bool RubbleSound;
    bool ThrowSound;

    void Update()
    {
        if (Berry.activeSelf == true  && BerrySound == false)
        {
            FindObjectOfType<AudioManager>().Play("Take Berry");
            BerrySound = true;
        }
        if (FullBucket.activeSelf == true && FullBucketSound == false)
        {
            FindObjectOfType<AudioManager>().Play("Place Bucket");
            FullBucketSound = true;
        }
        if (Axe.activeSelf == false && AxeSound == false)
        {
            FindObjectOfType<AudioManager>().Play("Pickup Axe");
            AxeSound = true;
        }
        if (Bucket.activeSelf == false && BucketSound == false)
        {
            FindObjectOfType<AudioManager>().Play("Pickup Bucket");
            BucketSound = true;
        }
        if (Rubble.activeSelf == false && RubbleSound == false)
        {
            FindObjectOfType<AudioManager>().Play("Pickup Rubble");
            RubbleSound = true;
        }
        if (Throw.activeSelf == true && ThrowSound == false)
        {
            FindObjectOfType<AudioManager>().Play("Throw Rubble");
            ThrowSound = true;
        }
    }
}
